import { Component } from '@angular/core';
import { ActivatedRoute, ActivationStart, Router } from '@angular/router';
import { AuthService } from '@workout-cms/core-data';
import { SearchBarFacade } from '@workout-cms/core-state';
import { Observable } from 'rxjs';

@Component({
  selector: 'workout-cms-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  public readonly isAuthenticated$: Observable<boolean> = this.auth
    .isAuthenticated$;
  public showSearchBar: boolean;
  constructor(
    private readonly searchBarFacade: SearchBarFacade,
    private auth: AuthService,
    private readonly route: Router
  ) {
    this.route.events.subscribe((event) => {
      if (event instanceof ActivationStart) {
        const data = event.snapshot.data;
        this.showSearchBar = data.showSearchBar;
      }
    });
  }

  search(query) {
    this.searchBarFacade.searchQuery(query);
  }

  logout() {
    this.auth.logout();
  }
}
