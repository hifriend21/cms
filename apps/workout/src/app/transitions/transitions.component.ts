import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'workout-cms-transitions',
  templateUrl: './transitions.component.html',
  styleUrls: ['./transitions.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransitionsComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
