import { ChangeDetectionStrategy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Transition } from '@workout-cms/api-interfaces';
import { SearchBarFacade, TransitionsFacade } from '@workout-cms/core-state';
import { Observable } from 'rxjs';

@Component({
  selector: 'workout-cms-transitions-table',
  templateUrl: './transitions-table.component.html',
  styleUrls: ['./transitions-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransitionsTableComponent implements OnInit {
  readonly loaded$: Observable<boolean> = this.transitionsFacade.loaded$;
  readonly allTransitions$: Observable<Transition[]> = this.transitionsFacade
    .allTransitions$;
  readonly searchQuery$: Observable<string> = this.searchBarFacade.query$;

  readonly searchControl = new FormControl('');

  readonly columns = ['name', 'number', 'id', 'actions'];

  constructor(
    private transitionsFacade: TransitionsFacade,
    private readonly searchBarFacade: SearchBarFacade,
    private router: Router
  ) {}

  ngOnInit() {
    this.transitionsFacade.loadTransitions();
  }

  remove(id: number) {
    return null;
  }

  navToEdit(id: number) {
    this.router.navigate(['/transitions/' + id]);
  }
}
