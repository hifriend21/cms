import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TaigaUiModule } from '@workout-cms/taiga-ui';
import { TokenInterceptor } from '@workout-cms/core-data';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreStateModule } from '@workout-cms/core-state';
import { UiNavigationModule } from '@workout-cms/ui-navigation';
import { UiTableModule } from '@workout-cms/ui-table';
import { UiToolbarModule } from '@workout-cms/ui-toolbar';
import { MaterialModule } from '@workout-cms/material';
import { SafeUrlPipeModule } from '@workout-cms/shared/pipes/safe-url-pipe';
import { ExerciseDetailsComponent } from './exercises/exercise-details/exercise-details.component';
import { ExercisesTableComponent } from './exercises/exercises-table/exercises-table/exercises-table.component';
import { TransitionsComponent } from './transitions/transitions.component';
import { TransitionsTableComponent } from './transitions/transitions-table/transitions-table/transitions-table.component';
import { ExerciseVideosComponent } from './exercises/exercise-videos/exercise-videos.component';
import { ExercisesEditComponent } from './exercises/exercises-edit/exercises-edit.component';
import { ExercisesFormComponent } from './exercises/exercises-form/exercises-form.component';
import { ExerciseAudioComponent } from './exercises/exercise-audio/exercise-audio.component';

@NgModule({
  declarations: [
    AppComponent,
    ExercisesTableComponent,
    ExerciseDetailsComponent,
    TransitionsComponent,
    TransitionsTableComponent,
    ExerciseAudioComponent,
    ExerciseVideosComponent,
    ExercisesEditComponent,
    ExercisesFormComponent,
  ],
  imports: [
    BrowserModule,
    TaigaUiModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    CoreStateModule,
    UiNavigationModule,
    UiToolbarModule,
    MaterialModule,
    UiTableModule,
    SafeUrlPipeModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
