import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '@workout-cms/ui-login';
import { AuthGuardService as AuthGuard } from '@workout-cms/core-data';
import { ExercisesTableComponent } from './exercises/exercises-table/exercises-table/exercises-table.component';
import { ExerciseDetailsComponent } from './exercises/exercise-details/exercise-details.component';
import { TransitionsComponent } from './transitions/transitions.component';
import { ExercisesEditComponent } from './exercises/exercises-edit/exercises-edit.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'exercises',
  },
  {
    path: 'exercises',
    component: ExercisesTableComponent,
    canActivate: [AuthGuard],
    data: { showSearchBar: true },
  },
  {
    path: 'exercises/:id',
    component: ExercisesEditComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'transitions',
    component: TransitionsComponent,
    canActivate: [AuthGuard],
    data: { showSearchBar: true },
  },
  // {
  //   path: 'transitions/:id',
  //   component: ExerciseDetailsComponent,
  //   canActivate: [AuthGuard],
  // },
  {
    path: 'login',
    component: LoginComponent,
  },
  { path: '**', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [],
})
export class AppRoutingModule {}
