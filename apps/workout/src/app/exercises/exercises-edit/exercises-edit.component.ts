import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject, combineLatest, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormGroup } from '@angular/forms';
import { Parameter, ExerciseDetails } from '@workout-cms/api-interfaces';
import {
  ExercisesService,
  ParametersService,
  ShootPlacesService,
} from '@workout-cms/core-data';
import { ExercisesForm } from '../exercises-form/providers/exercises-form';
import { ExercisesFormActions } from '../exercises-form/providers/exercises-form-actions';
import { ExercisesFormFactory } from '../exercises-form/providers/exercises-form-factory';
import { ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'workout-cms-exercises-edit',
  templateUrl: './exercises-edit.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    ExercisesFormFactory,
    {
      provide: ExercisesForm,
      useFactory: (factory: ExercisesFormFactory) => factory.create(),
      deps: [ExercisesFormFactory],
    },
    ExercisesFormActions,
  ],
})
export class ExercisesEditComponent implements OnInit, OnDestroy {
  private readonly exerciseId: string = this.activateRoute.snapshot.params.id;

  public form: FormGroup = this.exercisesForm.asFormGroup;
  public isLoading = new BehaviorSubject(true);
  private onDestroy$ = new Subject();

  constructor(
    private exercisesService: ExercisesService,
    // private store: ExercisesStoreService,
    // private uploadService: UploadService,
    private activateRoute: ActivatedRoute,
    private shootPlacesApiService: ShootPlacesService,
    private parametersService: ParametersService,
    private exercisesForm: ExercisesForm,
    private formActions: ExercisesFormActions
  ) {}

  ngOnInit(): void {
    combineLatest([
      this.parametersService.getAll(),
      this.exercisesService.getById(this.exerciseId),
    ])
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(([allParams, exercise]: [Parameter[], ExerciseDetails]) => {
        this.formActions.addParameterControlsEdit(
          exercise.param_options,
          allParams
        );
        exercise = this.formActions.mapKeys(exercise);
        this.formActions.patchValue(exercise);
        this.isLoading.next(false);
      });
  }

  save(): void {
    const formValue = JSON.parse(JSON.stringify(this.form.value));
    const exercise = this.formActions.filterNotUpdated(formValue);
    const formData = this.formActions.mapFormData(exercise);

    this.exercisesService
      .update(formData)
      .subscribe
      // (res) => this.uploadService.handleSuccess(res),
      // (err) => this.uploadService.handleError(err)
      ();
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    // this.store.exercise = null;
  }
}
