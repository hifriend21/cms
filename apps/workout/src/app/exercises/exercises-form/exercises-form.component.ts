import { Point } from '@angular/cdk/drag-drop';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ShootPlace, Parameter } from '@workout-cms/api-interfaces';
import {
  ParametersService,
  PointService,
  ShootPlacesService,
} from '@workout-cms/core-data';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ExercisesForm } from './providers/exercises-form';
import { ExercisesFormActions } from './providers/exercises-form-actions';
@Component({
  selector: 'workout-cms-exercises-form',
  templateUrl: './exercises-form.component.html',
  styleUrls: ['./exercises-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExercisesFormComponent {
  public readonly pointTree$: Observable<
    Point[]
  > = this.pointService.getPointTree();
  public readonly shootPlaces$: Observable<
    ShootPlace[]
  > = this.shootPlacesApiService.getAll();

  public readonly parameters$: Observable<
    Parameter[]
  > = this.parametersService
    .getAll()
    .pipe(tap((params) => this.formActions.addParameterControls(params)));

  public form: FormGroup = this.exercisesForm.asFormGroup;

  constructor(
    private pointService: PointService,
    private shootPlacesApiService: ShootPlacesService,
    private parametersService: ParametersService,
    private exercisesForm: ExercisesForm,
    private formActions: ExercisesFormActions
  ) {}
}
