import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ExercisesForm } from './exercises-form';

@Injectable()
export class ExercisesFormFactory {
  constructor(private formBuilder: FormBuilder) {}

  create() {
    const formGroup = this.createFormGroup();
    return new ExercisesForm(formGroup);
  }

  private createFormGroup() {
    return this.formBuilder.group({
      point_id: [null, [Validators.required]],
      number: [null, [Validators.required]],
      id: [null, [Validators.required]],
      practice_video: [null],
      entry_video: [null],
      exit_video: [null],
      param_options: this.formBuilder.array([]),
      shoot_place_uuid: [null, [Validators.required]],
      translations: this.formBuilder.group({
        ru: this.createTranslationGroup(),
        en: this.createTranslationGroup(),
      }),
    });
  }

  createTranslationGroup(): FormGroup {
    return this.formBuilder.group({
      name: [null, [Validators.required]],
      description: [null, [Validators.required]],
      start_audio: [null, [Validators.required]],
      middle_audio: [null, [Validators.required]],
      end_audio: [null, [Validators.required]],
      technique_audio: [null, [Validators.required]],
      entry_audio: [null, [Validators.required]],
      exit_audio: [null, [Validators.required]],
    });
  }
}
