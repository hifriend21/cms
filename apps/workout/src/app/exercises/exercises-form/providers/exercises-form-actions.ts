import { Injectable } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Exercise, Parameter } from '@workout-cms/api-interfaces';
import { BehaviorSubject } from 'rxjs';

import { ExercisesForm } from './exercises-form';

@Injectable()
export class ExercisesFormActions {
  language$ = new BehaviorSubject<string>('ru');
  constructor(private form: ExercisesForm, private formBuilder: FormBuilder) {}

  get newOption() {
    return new FormGroup({
      name: new FormControl(''),
    });
  }

  patchValue(exercise) {
    this.form.asFormGroup.patchValue({
      ...exercise,
      param_options: exercise.param_options || [],
    });
  }

  mapKeys(exercise) {
    const videos = ['practice', 'technique', 'exit', 'entry'];
    for (const key in exercise) {
      const hasKey = Object.prototype.hasOwnProperty.call(exercise, key);
      if (hasKey && videos.includes(key)) {
        exercise[key + '_video'] = exercise[key];
      }
    }
    return exercise;
  }
  changeLanguage(language: string) {
    this.language$.next(language);
  }
  // TODO refuck this shit
  filterNotUpdated(form): void {
    const objectFields = [
      'image',
      'entry_video',
      'entry_audio',
      'exit_audio',
      'technique_audio',
      'practice_video',
      'technique_video',
      'exit_video',
      'end_audio',
      'start_audio',
      'middle_audio',
      'video',
    ];

    // tslint:disable-next-line: forin
    for (const field in form) {
      if (objectFields.includes(field) && !(form[field] instanceof File)) {
        delete form[field];
      }
      if (typeof form[field] === 'object') {
        for (const innerField in form[field]) {
          if (
            objectFields.includes(innerField) &&
            typeof form[field][innerField] !== 'object'
          ) {
            delete form[field][innerField];
          } else {
            for (const inner in form[field][innerField]) {
              if (
                objectFields.includes(inner) &&
                typeof form[field][innerField][inner] !== 'object'
              ) {
                delete form[field][innerField][inner];
              }
            }
          }
        }
      }
    }
    return form;
  }

  addParameterControlsEdit(exerciseParams, allParams: Parameter[]) {
    if (!exerciseParams) {
      return this.addParameterControls(allParams);
    }
    const formParams = this.form.asFormGroup.get('param_options') as FormArray;

    const existingParams = exerciseParams.map((p) => p.param_id);
    for (const param of allParams) {
      if (!existingParams.includes(param.uuid)) {
        exerciseParams.push({ param_name: param.name, param_id: param.uuid });
      }
    }

    for (const exeriseParam of exerciseParams) {
      const newParam = this.parameterControlEdit();
      newParam.patchValue(exeriseParam);
      formParams.push(newParam);
    }
  }

  parameterControlEdit() {
    return this.formBuilder.group({
      param_id: [''],
      option_id: [''],
      option_name: [''],
      param_name: [''],
    });
  }

  addParameterControls(parameters: Parameter[]) {
    const params = this.form.asFormGroup.get('param_options') as FormArray;
    if (!params.length) {
      for (const param of parameters) {
        const newParam = this.parameterControl(param.uuid);
        params.push(newParam);
      }
    }
  }

  parameterControl(paramId: string) {
    return this.formBuilder.group({
      param_id: [paramId],
      option_id: [''],
    });
  }

  // *TODO: Refuck
  mapFormData(data): any {
    const formData = new FormData();
    for (const key in data) {
      // eslint-disable-next-line no-prototype-builtins
      if (data.hasOwnProperty(key)) {
        if (key === 'translations') {
          Object.keys(data.translations).forEach((translation) => {
            Object.keys(data.translations[translation]).forEach(
              (translationItem) => {
                if (data.translations[translation][translationItem]) {
                  formData.append(
                    `translations[${translation}][${translationItem}]`,
                    data.translations[translation][translationItem]
                  );
                }
              }
            );
          });
        } else {
          data[key] && key !== 'param_options'
            ? formData.append(key, data[key])
            : null;
        }
      }
    }
    data.param_options.forEach((param, i) => {
      if (param.param_id && param.option_id) {
        formData.append(`param_options[${i}][param_id]`, param.param_id);
        formData.append(`param_options[${i}][option_id]`, param.option_id);
      }
    });
    return formData;
  }
}
