import { ChangeDetectionStrategy } from '@angular/core';
import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { Exercise } from '@workout-cms/api-interfaces';

@Component({
  selector: 'workout-cms-exercise-videos',
  templateUrl: './exercise-videos.component.html',
  styleUrls: ['./exercise-videos.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExerciseVideosComponent implements OnInit {
  @Input() form: FormGroup;
  exercise: Exercise;
  video: AbstractControl;

  contentTabs = [
    { name: 'Практика', slug: 'practice' },
    { name: 'Вход', slug: 'entry' },
    { name: 'Выход', slug: 'exit' },
  ];

  constructor() {}

  ngOnInit(): void {
    this.video = this.form.value;
  }

  public uploadVideo(event, slug): void {
    const videoType = slug + '_video';
    const file = event?.target?.files[0];
    this.form.patchValue({
      [videoType]: file,
    });
  }
}
