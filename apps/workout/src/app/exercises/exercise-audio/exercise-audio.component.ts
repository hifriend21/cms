import { ChangeDetectionStrategy } from '@angular/core';
import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, AbstractControl } from '@angular/forms';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { BehaviorSubject } from 'rxjs';
import { ExercisesFormActions } from '../exercises-form/providers/exercises-form-actions';

type AudioItem = { name: string; slug: string };
type LanguageTab = { label: string; index: number; language: string };

@Component({
  selector: 'workout-cms-exercise-audio',
  templateUrl: './exercise-audio.component.html',
  styleUrls: ['./exercise-audio.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExerciseAudioComponent implements OnInit {
  @Input() form: FormGroup;

  currentLanguage$: BehaviorSubject<string> = this.formActions.language$;

  translations: AbstractControl;
  languageTabs: LanguageTab[] = [
    { label: 'Русский', index: 0, language: 'ru' },
    { label: 'Английский', index: 1, language: 'en' },
  ];

  audioItems: AudioItem[] = [
    { name: 'Начало практики', slug: 'start_audio' },
    { name: 'Середина практики', slug: 'middle_audio' },
    { name: 'Конец практики', slug: 'end_audio' },
    { name: 'Вход', slug: 'entry_audio' },
    { name: 'Выход', slug: 'exit_audio' },
  ];

  constructor(private formActions: ExercisesFormActions) {}

  ngOnInit(): void {
    console.log(this.form);
    this.translations = this.form.get('translations');
  }

  switchTab(e: MatTabChangeEvent): void {
    const currentLanguage = this.languageTabs[e.index].language;
    this.formActions.changeLanguage(currentLanguage);
  }

  removeAudio(type: string): void {
    const language = this.currentLanguage$.getValue();
    this.translations.patchValue({
      [language]: {
        [type]: null,
      },
    });
  }

  public uploadAudio(event, type: string): void {
    const language = this.currentLanguage$.getValue();
    const file = event?.target?.files[0];
    this.translations.patchValue({
      [language]: {
        [type]: file,
      },
    });
  }
}
