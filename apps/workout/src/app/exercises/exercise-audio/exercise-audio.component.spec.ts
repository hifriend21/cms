/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import 'intersection-observer';
import { ExerciseAudioComponent } from './exercise-audio.component';
import { ExercisesFormActions } from '../exercises-form/providers/exercises-form-actions';
import { ExercisesForm } from '../exercises-form/providers/exercises-form';
import { ExercisesFormFactory } from '../exercises-form/providers/exercises-form-factory';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '@workout-cms/material';
import { TaigaUiModule } from '@workout-cms/taiga-ui';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('ExerciseAudioComponent', () => {
  let component: ExerciseAudioComponent;
  let fixture: ComponentFixture<ExerciseAudioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ExerciseAudioComponent],
      imports: [
        BrowserAnimationsModule,
        ReactiveFormsModule,
        MaterialModule,
        TaigaUiModule,
      ],
      providers: [
        ExercisesFormFactory,
        {
          provide: ExercisesForm,
          useFactory: (factory: ExercisesFormFactory) => factory.create(),
          deps: [ExercisesFormFactory],
        },
        ExercisesFormActions,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExerciseAudioComponent);
    const form = fixture.debugElement.componentInstance;
    form.form = new FormGroup({
      translations: new FormGroup({
        ru: new FormControl(''),
        en: new FormControl(''),
      }),
    });
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
