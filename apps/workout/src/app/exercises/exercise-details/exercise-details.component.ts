import { Point } from '@angular/cdk/drag-drop';
import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  ViewEncapsulation,
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Exercise, Parameter, ShootPlace } from '@workout-cms/api-interfaces';
import { BehaviorSubject } from 'rxjs';
import { ExercisesFormActions } from '../exercises-form/providers/exercises-form-actions';

@Component({
  selector: 'workout-cms-exercise-details',
  templateUrl: './exercise-details.component.html',
  styleUrls: ['./exercise-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class ExerciseDetailsComponent implements OnInit {
  @Input() points: Point[];
  @Input() exercise: Exercise;
  @Input() isEditing: boolean;
  @Input() form: FormGroup;
  @Input() shootPlaces: ShootPlace[];
  @Input() parameters: Parameter[];
  @Input() isLoaded: boolean;

  public currentLanguage$: BehaviorSubject<string> = this.formActions.language$;
  public parameterControls: FormControl[];
  public hasParameters: boolean;

  constructor(private formActions: ExercisesFormActions) {}

  ngOnInit(): void {
    this.parameterControls = this.form.get('param_options')['controls'];
    this.hasParameters = this.parameterControls[0]?.value.param_name;
  }

  getParamOptions(paramId: string) {
    return this.parameters.find((param) => param.uuid === paramId);
  }
}
