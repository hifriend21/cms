import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreDataModule } from '@workout-cms/core-data';
import { CoreStateModule, ExercisesFacade } from '@workout-cms/core-state';
import { MaterialModule } from '@workout-cms/material';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ExerciseDetailsComponent } from './exercise-details.component';
import { RouterTestingModule } from '@angular/router/testing';
describe('ExerciseDetailsComponent', () => {
  let component: ExerciseDetailsComponent;
  let fixture: ComponentFixture<ExerciseDetailsComponent>;
  let de: DebugElement;
  let exercisesFacade: ExercisesFacade;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExerciseDetailsComponent],
      imports: [
        ReactiveFormsModule,
        FormsModule,
        MaterialModule,
        CoreStateModule,
        CoreDataModule,
        HttpClientTestingModule,
        RouterTestingModule,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExerciseDetailsComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    // exercisesFacade = TestBed.inject(ExercisesFacade);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should on select call exerciseFacade loadExercise', () => {
    const spy = jest.spyOn(exercisesFacade, 'loadExercise');
    // component.
  });
});

/**
 *describe('UsersComponent', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;
  let de: DebugElement;
  let usersFacade: UsersFacade;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UsersComponent, UserDetailsComponent, UsersListComponent],
      imports: [
        CoreDataModule,
        CoreStateModule,
        FormsModule,
        MaterialModule,
        HttpClientTestingModule,
        NoopAnimationsModule,
        RouterTestingModule,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    usersFacade = TestBed.inject(UsersFacade);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should on select call usersFacade selectUser', () => {
    const spy = jest.spyOn(usersFacade, 'selectUser');

    component.selectUser(mockUser);

    expect(spy).toHaveBeenCalledWith(mockUser.id);
  });

  describe('should on save call usersFacade', () => {
    it('updateUser', () => {
      const spy = jest.spyOn(usersFacade, 'updateUser');

      component.saveUser(mockUser);

      expect(spy).toHaveBeenCalledWith(mockUser);
    });

    it('createUser', () => {
      const spy = jest.spyOn(usersFacade, 'createUser');

      component.saveUser(mockEmptyUser);

      expect(spy).toHaveBeenCalledWith(mockEmptyUser);
    });
  });

  it('should on delete call usersFacade deleteUser', () => {
    const spy = jest.spyOn(usersFacade, 'deleteUser');

    component.deleteUser(mockUser);

    expect(spy).toHaveBeenCalledWith(mockUser);
  });
});

 */
