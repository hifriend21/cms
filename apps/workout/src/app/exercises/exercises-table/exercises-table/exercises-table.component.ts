import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Exercise } from '@workout-cms/api-interfaces';
import { ExercisesFacade, SearchBarFacade } from '@workout-cms/core-state';
import { Observable } from 'rxjs';

@Component({
  selector: 'workout-cms-exercises-table',
  templateUrl: './exercises-table.component.html',
  styleUrls: ['./exercises-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExercisesTableComponent implements OnInit {
  readonly loaded$: Observable<boolean> = this.exercisesFacade.loaded$;
  readonly allExercises$: Observable<Exercise[]> = this.exercisesFacade
    .allExercises$;
  readonly searchQuery$: Observable<string> = this.searchBarFacade.query$;

  readonly searchControl = new FormControl('');

  readonly columns = ['name', 'number', 'id', 'actions'];

  constructor(
    private exercisesFacade: ExercisesFacade,
    private readonly searchBarFacade: SearchBarFacade,
    private router: Router
  ) {}

  ngOnInit() {
    this.exercisesFacade.loadExercises();
  }

  remove(id: number) {
    return null;
  }

  navToEdit(id: number) {
    this.router.navigate(['/exercises/' + id]);
  }
}
