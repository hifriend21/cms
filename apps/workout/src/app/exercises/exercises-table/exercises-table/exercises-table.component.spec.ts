/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ExercisesTableComponent } from './exercises-table.component';

describe('ExercisesTableComponent', () => {
  let component: ExercisesTableComponent;
  let fixture: ComponentFixture<ExercisesTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExercisesTableComponent],
    }).compileComponents();
  });

  it('should create the component', () => {
    const fixture = TestBed.createComponent(ExercisesTableComponent);
    const component = fixture.componentInstance;
    expect(component).toBeTruthy();
  });
});
