module.exports = {
  projects: [
    '<rootDir>/apps/workout',
    '<rootDir>/libs/transitions',
    '<rootDir>/libs/exercises',
    '<rootDir>/libs/api-interfaces',
    '<rootDir>/libs/auth',
    '<rootDir>/libs/taiga-ui',
    '<rootDir>/libs/ui-login',
    '<rootDir>/libs/core-data',
    '<rootDir>/libs/core-state',
    '<rootDir>/libs/ui-navigation',
    '<rootDir>/libs/ui-toolbar',
    '<rootDir>/libs/material',
    '<rootDir>/libs/ui-table',
    '<rootDir>/libs/shared/pipes/safe-url-pipe',
  ],
};
