import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { Subject } from 'rxjs';

@Component({
  selector: 'workout-cms-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit, OnDestroy {
  @Output() searchQuery = new EventEmitter<{ query: string }>();
  readonly searchControl = new FormControl('');
  private readonly destroy$ = new Subject();

  constructor(private readonly router: Router) {
  }

  ngOnInit() {
    this._initializeRouterSubscription();
    this._initializeQuerySubscription();
  }

  private _initializeRouterSubscription(): void {
    this.router.events
      .pipe(takeUntil(this.destroy$))
      .subscribe((event: RouterEvent) => {
        if (event instanceof NavigationEnd) {
          this._clear();
        }
      });
  }

  private _clear(): void {
    this.searchControl.setValue('');
    this._emitQuery('');
  }

  private _initializeQuerySubscription(): void {
    this.searchControl.valueChanges
      .pipe(
        debounceTime(500),
        takeUntil(this.destroy$)
      )
      .subscribe(this._emitQuery);
  }

  private _emitQuery = (query: string) => {
    this.searchQuery.emit({ query });
  };

  ngOnDestroy() {
    this.destroy$.next();
  }
}
