import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { TaigaUiModule } from '@workout-cms/taiga-ui';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [ToolbarComponent],
  imports: [CommonModule, TaigaUiModule, FormsModule, ReactiveFormsModule],
  exports: [ToolbarComponent],
})
export class UiToolbarModule {}
