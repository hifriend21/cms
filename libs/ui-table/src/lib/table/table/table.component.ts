import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import {
  Subject,
  combineLatest,
  from,
  BehaviorSubject,
  Observable,
} from 'rxjs';
import {
  startWith,
  filter,
  map,
  toArray,
  switchMap,
  debounceTime,
  share,
} from 'rxjs/operators';
import { SearchBarFacade } from '@workout-cms/core-state';
import { defaultSort, TuiComparator } from '@taiga-ui/addon-table';
import { of } from 'rxjs';
@Component({
  selector: 'workout-cms-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TableComponent {
  @Input() loaded: boolean;
  @Input()
  set items(allItems) {
    this.allItems$.next(allItems);
  }

  @Output() navToEdit: EventEmitter<boolean> = new EventEmitter();
  @Output() removeItem: EventEmitter<number> = new EventEmitter();
  @Input() columns = ['name', 'number', 'id', 'actions'];
  readonly allItems$ = new BehaviorSubject([]);

  private readonly size$ = new Subject<number>();
  private readonly page$ = new Subject<number>();

  readonly direction$ = new BehaviorSubject<-1 | 1>(1);

  readonly searchQuery$: Observable<string> = this.searchBarFacade.query$;
  entities$ = combineLatest([
    this.allItems$,
    this.size$.pipe(startWith(10)),
    this.page$.pipe(startWith(0)),
    this.searchQuery$,
    this.direction$,
  ]).pipe(
    debounceTime(0),
    switchMap((query) => this.getData(...query).pipe(startWith(null))),
    share()
  );
  readonly data$ = this.entities$.pipe(startWith([]));
  readonly total$ = this.allItems$.pipe(
    map(({ length }) => length),
    startWith(1)
  );

  constructor(private searchBarFacade: SearchBarFacade) {}

  getData(items, size, page, query, direction) {
    const start = page * size;
    const end = start + size;

    const result = [...items]
      .sort(sortBy('grerg', direction))
      .filter((item: any) =>
        query ? item.name.toLowerCase().startsWith(query.toLowerCase()) : true
      )
      .map((item, i) => (i >= start && i <= end ? item : null))
      .filter(Boolean);

    return of(result);
  }

  onDirection(direction: -1 | 1) {
    this.direction$.next(direction);
  }

  onSize(size: number) {
    this.size$.next(size);
  }

  onPage(page: number) {
    this.page$.next(page);
  }
}

function sortBy(key, direction: -1 | 1): TuiComparator<any> {
  return (a, b) => direction * defaultSort(a[key], b[key]);
}
