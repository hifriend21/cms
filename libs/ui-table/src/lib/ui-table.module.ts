import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './table/table/table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TaigaUiModule } from '@workout-cms/taiga-ui';
@NgModule({
  declarations: [TableComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, TaigaUiModule],
  exports: [TableComponent],
})
export class UiTableModule {}
