import { async, TestBed } from '@angular/core/testing';
import { UiTableModule } from './ui-table.module';

describe('UiTableModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [UiTableModule],
    }).compileComponents();
  }));

  // TODO: Add real tests here.
  //
  // NB: This particular test does not do anything useful.
  //     It does NOT check for correct instantiation of the module.
  it('should have a module definition', () => {
    expect(UiTableModule).toBeDefined();
  });
});
