import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RootStoreConfig, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromExercises from './exercises/exercises.reducer';
import { ExercisesEffects } from './exercises/exercises.effects';
import { ExercisesFacade } from './exercises/exercises.facade';
import { reducers } from '.';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { SearchBarFacade } from './searchbar/searchbar.facade';
import * as fromTransitions from './transitions/transitions.reducer';
import { TransitionsEffects } from './transitions/transitions.effects';
import { TransitionsFacade } from './transitions/transitions.facade';

const STORE_NAME = 'workout-cms-store';
const storeConfig: RootStoreConfig<any> = {
  runtimeChecks: {
    strictActionImmutability: true,
    strictActionSerializability: true,
    strictStateImmutability: true,
    strictStateSerializability: true,
  },
};

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forRoot(reducers, storeConfig),
    EffectsModule.forRoot([ExercisesEffects, TransitionsEffects]),
    StoreDevtoolsModule.instrument({ maxAge: 25, name: STORE_NAME }),
    StoreRouterConnectingModule.forRoot({ stateKey: 'router' }),
    // StoreModule.forFeature(
    //   fromTransitions.TRANSITIONS_FEATURE_KEY,
    //   fromTransitions.reducer
    // ),
    EffectsModule.forFeature([TransitionsEffects]),
  ],
  providers: [ExercisesFacade, SearchBarFacade, TransitionsFacade],
})
export class CoreStateModule {}
