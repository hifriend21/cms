import { NgModule } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { readFirst } from '@nrwl/angular/testing';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule, Store } from '@ngrx/store';

import { NxModule } from '@nrwl/angular';

import { TransitionsEntity } from './transitions.models';
import { TransitionsEffects } from './transitions.effects';
import { TransitionsFacade } from './transitions.facade';

import * as TransitionsSelectors from './transitions.selectors';
import * as TransitionsActions from './transitions.actions';
import {
  TRANSITIONS_FEATURE_KEY,
  State,
  initialState,
  reducer,
} from './transitions.reducer';

interface TestSchema {
  transitions: State;
}

describe('TransitionsFacade', () => {
  let facade: TransitionsFacade;
  let store: Store<TestSchema>;
  const createTransitionsEntity = (id: string, name = '') =>
    ({
      id,
      name: name || `name-${id}`,
    } as TransitionsEntity);

  beforeEach(() => {});

  describe('used in NgModule', () => {
    beforeEach(() => {
      @NgModule({
        imports: [
          StoreModule.forFeature(TRANSITIONS_FEATURE_KEY, reducer),
          EffectsModule.forFeature([TransitionsEffects]),
        ],
        providers: [TransitionsFacade],
      })
      class CustomFeatureModule {}

      @NgModule({
        imports: [
          NxModule.forRoot(),
          StoreModule.forRoot({}),
          EffectsModule.forRoot([]),
          CustomFeatureModule,
        ],
      })
      class RootModule {}
      TestBed.configureTestingModule({ imports: [RootModule] });

      store = TestBed.inject(Store);
      facade = TestBed.inject(TransitionsFacade);
    });

    /**
     * The initially generated facade::loadAll() returns empty array
     */
    it('loadAll() should return empty list with loaded == true', async (done) => {
      try {
        let list = await readFirst(facade.allTransitions$);
        let isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(0);
        expect(isLoaded).toBe(false);

        facade.init();

        list = await readFirst(facade.allTransitions$);
        isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(0);
        expect(isLoaded).toBe(true);

        done();
      } catch (err) {
        done.fail(err);
      }
    });

    /**
     * Use `loadTransitionsSuccess` to manually update list
     */
    it('allTransitions$ should return the loaded list; and loaded flag == true', async (done) => {
      try {
        let list = await readFirst(facade.allTransitions$);
        let isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(0);
        expect(isLoaded).toBe(false);

        store.dispatch(
          TransitionsActions.loadTransitionsSuccess({
            transitions: [
              createTransitionsEntity('AAA'),
              createTransitionsEntity('BBB'),
            ],
          })
        );

        list = await readFirst(facade.allTransitions$);
        isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(2);
        expect(isLoaded).toBe(true);

        done();
      } catch (err) {
        done.fail(err);
      }
    });
  });
});
