import { TestBed, async } from '@angular/core/testing';

import { Observable } from 'rxjs';

import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';

import { NxModule, DataPersistence } from '@nrwl/angular';
import { hot } from '@nrwl/angular/testing';

import { TransitionsEffects } from './transitions.effects';
import * as TransitionsActions from './transitions.actions';

describe('TransitionsEffects', () => {
  let actions: Observable<any>;
  let effects: TransitionsEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NxModule.forRoot()],
      providers: [
        TransitionsEffects,
        DataPersistence,
        provideMockActions(() => actions),
        provideMockStore(),
      ],
    });

    effects = TestBed.inject(TransitionsEffects);
  });

  describe('init$', () => {
    it('should work', () => {
      actions = hot('-a-|', { a: TransitionsActions.init() });

      const expected = hot('-a-|', {
        a: TransitionsActions.loadTransitionsSuccess({ transitions: [] }),
      });

      expect(effects.init$).toBeObservable(expected);
    });
  });
});
