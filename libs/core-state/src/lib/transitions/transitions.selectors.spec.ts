import { TransitionsEntity } from './transitions.models';
import { State, transitionsAdapter, initialState } from './transitions.reducer';
import * as TransitionsSelectors from './transitions.selectors';

describe('Transitions Selectors', () => {
  const ERROR_MSG = 'No Error Available';
  const getTransitionsId = (it) => it['id'];
  const createTransitionsEntity = (id: string, name = '') =>
    ({
      id,
      name: name || `name-${id}`,
    } as TransitionsEntity);

  let state;

  beforeEach(() => {
    state = {
      transitions: transitionsAdapter.setAll(
        [
          createTransitionsEntity('PRODUCT-AAA'),
          createTransitionsEntity('PRODUCT-BBB'),
          createTransitionsEntity('PRODUCT-CCC'),
        ],
        {
          ...initialState,
          selectedId: 'PRODUCT-BBB',
          error: ERROR_MSG,
          loaded: true,
        }
      ),
    };
  });

  describe('Transitions Selectors', () => {
    it('getAllTransitions() should return the list of Transitions', () => {
      const results = TransitionsSelectors.getAllTransitions(state);
      const selId = getTransitionsId(results[1]);

      expect(results.length).toBe(3);
      expect(selId).toBe('PRODUCT-BBB');
    });

    it('getSelected() should return the selected Entity', () => {
      const result = TransitionsSelectors.getSelected(state);
      const selId = getTransitionsId(result);

      expect(selId).toBe('PRODUCT-BBB');
    });

    it("getTransitionsLoaded() should return the current 'loaded' status", () => {
      const result = TransitionsSelectors.getTransitionsLoaded(state);

      expect(result).toBe(true);
    });

    it("getTransitionsError() should return the current 'error' state", () => {
      const result = TransitionsSelectors.getTransitionsError(state);

      expect(result).toBe(ERROR_MSG);
    });
  });
});
