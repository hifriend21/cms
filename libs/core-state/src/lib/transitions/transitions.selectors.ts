import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  TRANSITIONS_FEATURE_KEY,
  TransitionsState as State,
  TransitionsPartialState,
  transitionsAdapter,
} from './transitions.reducer';

// Lookup the 'Transitions' feature state managed by NgRx
export const getTransitionsState = createFeatureSelector<
  TransitionsPartialState,
  State
>(TRANSITIONS_FEATURE_KEY);

const { selectAll, selectEntities } = transitionsAdapter.getSelectors();

export const getTransitionsLoaded = createSelector(
  getTransitionsState,
  (state: State) => state.loaded
);

export const getTransitionsError = createSelector(
  getTransitionsState,
  (state: State) => state.error
);

export const getAllTransitions = createSelector(
  getTransitionsState,
  (state: State) => selectAll(state)
);

export const getTransitionsEntities = createSelector(
  getTransitionsState,
  (state: State) => selectEntities(state)
);

export const getSelectedTransitionId = createSelector(
  getTransitionsState,
  (state: State) => state.selectedId
);

export const getSelectedTransition = createSelector(
  getTransitionsEntities,
  getSelectedTransitionId,
  (entities, selectedId) => selectedId && entities[selectedId]
);
