import { Injectable } from '@angular/core';

import { select, Store, Action } from '@ngrx/store';

import * as TransitionsActions from './transitions.actions';
import * as TransitionsFeature from './transitions.reducer';
import * as TransitionsSelectors from './transitions.selectors';

@Injectable()
export class TransitionsFacade {
  /**
   * Combine pieces of state using createSelector,
   * and expose them as observables through the facade.
   */
  loaded$ = this.store.pipe(select(TransitionsSelectors.getTransitionsLoaded));
  allTransitions$ = this.store.pipe(
    select(TransitionsSelectors.getAllTransitions)
  );
  selectedTransitions$ = this.store.pipe(
    select(TransitionsSelectors.getSelectedTransition)
  );

  constructor(private store: Store) {}

  /**
   * Use the initialization action to perform one
   * or more tasks in your Effects.
   */
  loadTransitions() {
    this.store.dispatch(TransitionsActions.loadTransitions());
  }
}
