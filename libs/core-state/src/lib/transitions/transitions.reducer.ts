import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import * as TransitionsActions from './transitions.actions';
import { TransitionsEntity } from './transitions.models';
import { Transition } from '@workout-cms/api-interfaces';

export const TRANSITIONS_FEATURE_KEY = 'transitions';

export interface TransitionsState extends EntityState<Transition> {
  selectedId?: string | number; // which Transitions record has been selected
  loaded: boolean; // has the Transitions list been loaded
  error?: string | null; // last known error (if any)
}

export interface TransitionsPartialState {
  readonly [TRANSITIONS_FEATURE_KEY]: TransitionsState;
}

export const transitionsAdapter: EntityAdapter<Transition> = createEntityAdapter<Transition>();

export const initialTransitionsState: TransitionsState = transitionsAdapter.getInitialState(
  {
    // set initial required properties
    loaded: false,
  }
);

const _transitionsReducer = createReducer(
  initialTransitionsState,
  // on(TransitionsActions.init, (state) => ({
  //   ...state,
  //   loaded: false,
  //   error: null,
  // })),
  on(TransitionsActions.loadTransitionsSuccess, (state, { transitions }) =>
    transitionsAdapter.setAll(transitions, { ...state, loaded: true })
  ),
  on(TransitionsActions.loadTransitionsFailure, (state, { error }) => ({
    ...state,
    error,
  }))
);

export function transitionsReducer(
  state: TransitionsState | undefined,
  action: Action
) {
  return _transitionsReducer(state, action);
}
