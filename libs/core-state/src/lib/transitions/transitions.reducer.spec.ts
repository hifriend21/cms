import { TransitionsEntity } from './transitions.models';
import * as TransitionsActions from './transitions.actions';
import { State, initialState, reducer } from './transitions.reducer';

describe('Transitions Reducer', () => {
  const createTransitionsEntity = (id: string, name = '') =>
    ({
      id,
      name: name || `name-${id}`,
    } as TransitionsEntity);

  beforeEach(() => {});

  describe('valid Transitions actions', () => {
    it('loadTransitionsSuccess should return set the list of known Transitions', () => {
      const transitions = [
        createTransitionsEntity('PRODUCT-AAA'),
        createTransitionsEntity('PRODUCT-zzz'),
      ];
      const action = TransitionsActions.loadTransitionsSuccess({ transitions });

      const result: State = reducer(initialState, action);

      expect(result.loaded).toBe(true);
      expect(result.ids.length).toBe(2);
    });
  });

  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
