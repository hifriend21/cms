/**
 * Interface for the 'Transitions' data
 */
export interface TransitionsEntity {
  id: string | number; // Primary ID
}
