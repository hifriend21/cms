import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { fetch } from '@nrwl/angular';
import * as TransitionsActions from './Transitions.actions';
import { Transition } from '@workout-cms/api-interfaces';
import { TransitionsService } from '@workout-cms/core-data';
import { map } from 'rxjs/operators';

@Injectable()
export class TransitionsEffects {
  loadTransitions$ = createEffect(() =>
    this.actions$.pipe(
      ofType(TransitionsActions.loadTransitions),
      fetch({
        run: (action) => {
          return this.TransitionsService.getAll().pipe(
            map((transitions: Transition[]) =>
              TransitionsActions.loadTransitionsSuccess({ transitions })
            )
          );
        },

        onError: (action, error) => {
          console.error('Error', error);
          return TransitionsActions.loadTransitionsFailure({ error });
        },
      })
    )
  );

  constructor(
    private actions$: Actions,
    private TransitionsService: TransitionsService
  ) {}
}
