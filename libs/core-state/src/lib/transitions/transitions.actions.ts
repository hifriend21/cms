import { createAction, props } from '@ngrx/store';
import { Transition } from '@workout-cms/api-interfaces';

// Load Transitions
export const loadTransitions = createAction('[Transitions Page] Init');

export const loadTransitionsSuccess = createAction(
  '[Transitions/API] Load Transitions Success',
  props<{ transitions: Transition[] }>()
);

export const loadTransitionsFailure = createAction(
  '[Transitions/API] Load Transitions Failure',
  props<{ error: any }>()
);
