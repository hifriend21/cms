import * as fromExercises from './exercises/exercises.reducer';
import * as fromSearchbar from './searchbar/searchbar.reducer';
import * as fromTransitions from './transitions/transitions.reducer';
import * as fromRouter from '@ngrx/router-store';
import { Params } from '@angular/router';
import { ActionReducerMap } from '@ngrx/store';

export interface RouterStateUrl {
  url: string;
  queryParams: Params;
  params: Params;
}

export interface AppState {
  router: fromRouter.RouterReducerState<RouterStateUrl>;
  [fromExercises.EXERCISES_FEATURE_KEY]: fromExercises.ExercisesState;
  [fromSearchbar.SEARCHBAR_FEATURE_KEY]: fromSearchbar.SearchBarState;
  [fromTransitions.TRANSITIONS_FEATURE_KEY]: fromTransitions.TransitionsState;
}

export const reducers: ActionReducerMap<AppState> = {
  router: fromRouter.routerReducer,
  [fromExercises.EXERCISES_FEATURE_KEY]: fromExercises.exercisesReducer,
  [fromSearchbar.SEARCHBAR_FEATURE_KEY]: fromSearchbar.searchbarReducer,
  [fromTransitions.TRANSITIONS_FEATURE_KEY]: fromTransitions.transitionsReducer,
};
