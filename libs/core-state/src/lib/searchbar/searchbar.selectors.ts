import { createFeatureSelector, createSelector } from '@ngrx/store';
import { selectRouteParams } from '../router/router.selectors';
import {
  searchbarAdapter,
  SearchBarState,
  SEARCHBAR_FEATURE_KEY,
} from './searchbar.reducer';

const { selectAll, selectEntities } = searchbarAdapter.getSelectors();

export const getSearchBarState = createFeatureSelector<SearchBarState>(
  SEARCHBAR_FEATURE_KEY
);

export const getQuery = createSelector(
  getSearchBarState,

  (state: SearchBarState) => state.query
);

