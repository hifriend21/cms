import { Injectable } from '@angular/core';

import { select, Store } from '@ngrx/store';

import * as SearchBarSelectors from './searchbar.selectors';
import * as SearchBarActions from './searchbar.actions';
@Injectable()
export class SearchBarFacade {
  query$ = this.store.pipe(select(SearchBarSelectors.getQuery));

  constructor(private store: Store) {}

  searchQuery(query: string) {
    this.store.dispatch(SearchBarActions.search(query));
  }
}
