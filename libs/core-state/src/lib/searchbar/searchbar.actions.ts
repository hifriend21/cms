import { createAction, props } from '@ngrx/store';

export const search = createAction('[Exercises Page] Searching', props<any>());
