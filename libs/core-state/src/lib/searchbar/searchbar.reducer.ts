import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import * as SearchBarActions from './searchbar.actions';

type SearchBarQuery = string;

export const SEARCHBAR_FEATURE_KEY = 'searchbar';

export interface SearchBarState extends EntityState<SearchBarQuery> {
  query: string;
}

export const searchbarAdapter: EntityAdapter<SearchBarQuery> = createEntityAdapter<SearchBarQuery>();

export const initialState: SearchBarState = searchbarAdapter.getInitialState({
  // set initial required properties
  query: '',
});

const _searchbarReducer = createReducer(
  initialState,
  on(SearchBarActions.search, (state, { query }) =>
    // searchbarAdapter.setAll(query, { ...state })
    ({
      ...state,
      query,
    })
  )
);

export function searchbarReducer(
  state: SearchBarState | undefined,
  action: Action
) {
  return _searchbarReducer(state, action);
}
