import { createAction, props } from '@ngrx/store';
import { Exercise, ExerciseDetails } from '@workout-cms/api-interfaces';

// Load exercises
export const loadExercises = createAction('[Exercises Page] Init');

export const loadExercisesSuccess = createAction(
  '[Exercises/API] Load Exercises Success',
  props<{ exercises: Exercise[] }>()
);

export const loadExercisesFailure = createAction(
  '[Exercises/API] Load Exercises Failure',
  props<{ error: any }>()
);

// Load exercise

export const loadExercise = createAction(
  '[Exercises  ] Load exercise',
  props<{ exerciseId: string }>()
);

export const loadExerciseSuccess = createAction(
  '[Exercises/API] Load Exercise Success',
  props<{ exercise: ExerciseDetails }>()
);

export const loadExerciseFailure = createAction(
  '[Exercises/API] Load Exercise Failure',
  props<{ error: any }>()
);
