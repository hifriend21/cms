import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import * as ExercisesActions from './exercises.actions';
import { Exercise, ExerciseDetails } from '@workout-cms/api-interfaces';

export const EXERCISES_FEATURE_KEY = 'exercises';
// eslint-disable-next-line @typescript-eslint/no-explicit-any
type TODO = any;
export interface ExercisesState extends EntityState<Exercise> {
  selectedId?: string | number; // which Exercises record has been selected
  // esercises: Exercise[];
  loaded: boolean; // has the Exercises list been loaded
  error?: string | null; // last known error (if any)
}

export interface ExercisesPartialState {
  readonly [EXERCISES_FEATURE_KEY]: ExercisesState;
}

export const exercisesAdapter: EntityAdapter<TODO> = createEntityAdapter<TODO>();

export const initialState: ExercisesState = exercisesAdapter.getInitialState({
  // set initial required properties

  loaded: false,
});

const _exercisesReducer = createReducer(
  initialState,
  on(ExercisesActions.loadExercises, (state) => ({
    ...state,
    loaded: false,
    error: null,
  })),
  on(ExercisesActions.loadExercisesSuccess, (state, { exercises }) =>
    exercisesAdapter.setAll(exercises, { ...state, loaded: true })
  ),
  on(ExercisesActions.loadExercisesFailure, (state, { error }) => ({
    ...state,
    error,
  })),
  on(ExercisesActions.loadExercise, (state) => ({
    ...state,
    loaded: false,
    error: null,
  })),
  on(ExercisesActions.loadExerciseSuccess, (state, { exercise }) =>
    exercisesAdapter.upsertOne(exercise, {
      ...state,
      loaded: true,
      selectedId: exercise.id,
    })
  ),
  on(ExercisesActions.loadExerciseFailure, (state, { error }) => ({
    ...state,
    error,
  }))
);

export function exercisesReducer(
  state: ExercisesState | undefined,
  action: Action
) {
  return _exercisesReducer(state, action);
}
