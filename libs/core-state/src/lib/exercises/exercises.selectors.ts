import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  EXERCISES_FEATURE_KEY,
  ExercisesState,
  ExercisesPartialState,
  exercisesAdapter,
} from './exercises.reducer';

// Lookup the 'Exercises' feature state managed by NgRx
export const getExercisesState = createFeatureSelector<
  ExercisesPartialState,
  ExercisesState
>(EXERCISES_FEATURE_KEY);

const { selectAll, selectEntities } = exercisesAdapter.getSelectors();

export const getExercisesLoaded = createSelector(
  getExercisesState,
  (state: ExercisesState) => state.loaded
);

export const getExercisesError = createSelector(
  getExercisesState,
  (state: ExercisesState) => state.error
);

export const getAllExercises = createSelector(
  getExercisesState,
  (state: ExercisesState) => selectAll(state)
);

export const getExercisesEntities = createSelector(
  getExercisesState,
  (state: ExercisesState) => selectEntities(state)
);

export const getSelectedId = createSelector(
  getExercisesState,
  (state: ExercisesState) => state.selectedId
);

export const getSelected = createSelector(
  getExercisesEntities,
  getSelectedId,
  (entities, selectedId) => selectedId && entities[selectedId]
);

export const getSelectedExerciseId = createSelector(
  getExercisesState,
  (state: ExercisesState) => state.selectedId
);

export const getSelectedExercise = createSelector(
  getExercisesEntities,
  getSelectedExerciseId,
  (entities, selectedId) => {
    // const emptyLesson: Lesson = {
    //   id: null,
    //   title: '',
    //   description: '',
    //   videoUri: '',
    //   courseId: null,
    // };

    return selectedId ? entities[selectedId] : {};
  }
);
