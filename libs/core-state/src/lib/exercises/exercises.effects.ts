import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { fetch } from '@nrwl/angular';
import * as ExercisesActions from './exercises.actions';
import { Exercise, ExerciseDetails } from '@workout-cms/api-interfaces';
import { ExercisesService } from '@workout-cms/core-data';
import { map } from 'rxjs/operators';

@Injectable()
export class ExercisesEffects {
  loadExercises$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ExercisesActions.loadExercises),
      fetch({
        run: (action) => {
          return this.exercisesService
            .getAll()
            .pipe(
              map((exercises: Exercise[]) =>
                ExercisesActions.loadExercisesSuccess({ exercises })
              )
            );
        },

        onError: (action, error) => {
          console.error('Error', error);
          return ExercisesActions.loadExercisesFailure({ error });
        },
      })
    )
  );

  loadExercise$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ExercisesActions.loadExercise),
      fetch({
        run: (action) => {
          return this.exercisesService
            .getById(action.exerciseId)
            .pipe(
              map((exercise: ExerciseDetails) =>
                ExercisesActions.loadExerciseSuccess({ exercise })
              )
            );
        },

        onError: (action, error) => {
          console.error('Error', error);
          return ExercisesActions.loadExerciseFailure({ error });
        },
      })
    )
  );

  constructor(
    private actions$: Actions,
    private exercisesService: ExercisesService
  ) {}
}
