/**
 * Interface for the 'Exercises' data
 */
export interface ExercisesEntity {
  id: string | number; // Primary ID
}
