import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'workout-cms-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent {
  @Output() logout = new EventEmitter();
}
