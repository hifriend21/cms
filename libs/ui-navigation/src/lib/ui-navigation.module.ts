import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NavigationComponent } from './navigation/navigation.component';
import { TaigaUiModule } from '@workout-cms/taiga-ui';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [NavigationComponent],
  imports: [CommonModule, TaigaUiModule, RouterModule],
  exports: [NavigationComponent],
})
export class UiNavigationModule {}
