// export interface BaseEntity {

// }

export interface Exercise {
  image: string;
  name: string;
  description: string;
  practice_video: string;
  id: number;
  technique_video: string;
  number: string;
  parent_id: number;
  point_id: number;
  to_point_id: number;
  param_options: any;
  from_point_id: number;
  shoot_place_number: number;
  entry_video?: string;
  currentLanguage?: string;
  exit_video?: string;
  translations: ExerciseTranslations;
}
export interface ExerciseDetails {
  id: number;
  point_id: number;
  name: string;
  number: string;
  practice: ExercisePractice;
  entry: ExerciseEntry;
  exit: ExerciseExit;
  translations: ExerciseTranslations;
  shoot_place_uuid: string;
  shoot_place_number: number;
  param_options: any;
}

export interface ExerciseTranslations {
  [key: string]: {
    description: string;
    name: string;
    start_audio?: string;
    middle_audio?: string;
    end_audio?: string;
    technique_audio?: string;
    exit_audio?: string;
    entry_audio?: string;
  };
}
export interface ExercisePractice {
  video: string;
  duration: number;
  start_audio: string;
  middle_audio?: string;
  end_audio?: string;
}

export interface ExerciseEntry {
  video: string;
  duration: number;
  audio: string;
}

export interface ExerciseExit {
  video: string;
  duration: number;
  audio: string;
}

export interface Language {
  [key: string]: { audio: string | File };
}

export interface Transition {
  id: number;
  number: string;
  name: string;
  video: string | File;
  video_duration: number;
  shoot_place_number: number;
  shoot_place_uuid: string;
  from_point: { id: number; name: string };
  to_point: { id: number; name: string };
  translations: Language;
}

export interface ShootPlace {
  uuid: string;
  number: number;
  name: string;
}

export interface Parameter {
  uuid?: string;
  name: string;
  options: ParameterOption[];
}
export interface ParameterOption {
  uuid?: string;
  name: string;
}
