import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '@env/environment';
import { ShootPlace } from '@workout-cms/api-interfaces';

@Injectable({
  providedIn: 'root',
})
export class ShootPlacesService {
  model = 'shoot_place';

  constructor(private http: HttpClient) {}

  getAll(): Observable<ShootPlace[]> {
    return this.http.get<ShootPlace[]>(`${this.getUrl()}`);
  }

  getById(id: string): Observable<ShootPlace> {
    return this.http.get<ShootPlace>(`${this.getUrlWithId(id)}`);
  }

  create(createShootPlaceRequest: {
    number: number;
    name: string;
  }): Observable<ShootPlace> {
    return this.http.post<ShootPlace>(
      `${this.getUrl()}`,
      createShootPlaceRequest
    );
  }

  delete(id: string): Observable<void> {
    return this.http.delete<void>(`${this.getUrlWithId(id)}`);
  }

  update(updateRequest: FormData): Observable<void> {
    const id = updateRequest.get('uiid');
    return this.http.put<void>(`${this.getUrlWithId(id)}`, updateRequest);
  }

  private getUrl() {
    return `${environment.apiEndpoint}${this.model}`;
  }

  private getUrlWithId(id) {
    return `${this.getUrl()}/${id}`;
  }
}
