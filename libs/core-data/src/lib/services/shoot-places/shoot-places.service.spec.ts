/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ShootPlacesService } from './shoot-places.service';

describe('Service: ShootPlaces', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShootPlacesService]
    });
  });

  it('should ...', inject([ShootPlacesService], (service: ShootPlacesService) => {
    expect(service).toBeTruthy();
  }));
});
