/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { TransitionsService } from './transitions.service';

describe('Service: Transitions', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TransitionsService]
    });
  });

  it('should ...', inject([TransitionsService], (service: TransitionsService) => {
    expect(service).toBeTruthy();
  }));
});
