import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Transition } from '@workout-cms/api-interfaces';
import { Observable } from 'rxjs';
import { CreateExerciseRequest } from '../exercises/exercises.service';

@Injectable({
  providedIn: 'root',
})
export class TransitionsService {
  model = 'transition';

  constructor(private http: HttpClient) {}

  create(createRequest: CreateExerciseRequest): Observable<void> {
    return this.http.post<void>(`${this.getUrl()}`, createRequest);
  }

  update(updateRequest: FormData): Observable<void> {
    const id = updateRequest.get('id');
    return this.http.put<void>(`${this.getUrlWithId(id)}`, updateRequest);
  }

  getAll(): Observable<Transition[]> {
    return this.http.get<Transition[]>(`${this.getUrl()}`);
  }

  getById(id: string): Observable<Transition> {
    return this.http.get<Transition>(`${this.getUrlWithId(id)}`);
  }

  delete(id: number): Observable<void> {
    return this.http.delete<void>(`${this.getUrlWithId(id)}`);
  }

  private getUrl() {
    return `${environment.apiEndpoint}${this.model}`;
  }

  private getUrlWithId(id) {
    return `${this.getUrl()}/${id}`;
  }
}
