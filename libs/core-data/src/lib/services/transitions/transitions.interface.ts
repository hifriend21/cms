export interface CreateTransitionRequest {
  name: string;
  number: number;
  parent_id: number;
  to_point_id: number;
  from_point_id: number;
  translations: { [key: string]: { audio: string } };
  video?: string;
}

export interface UpdateTransitionRequest {
  name: string;
  id: string;
  number: number;
  video?: string;
  translations?: [{ key: { audio: string } }];
}
