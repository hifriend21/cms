import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from '@env/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  isAuthenticated = new BehaviorSubject(false);
  isAuthenticated$ = this.isAuthenticated.asObservable();

  constructor(private http: HttpClient, private router: Router) {
    this.setToken(this.getToken());
  }

  login(loginRequest): Observable<any> {
    return this.http.post(
      `${environment.apiEndpoint}cms_user/login`,
      loginRequest
    );
  }

  logout() {
    this.setToken('');
    this.router.navigateByUrl('/login');
  }

  setToken(token: string) {
    localStorage.setItem('token', token);
    this.isAuthenticated.next(!!token);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  check() {
    return this.isAuthenticated.value;
  }
}
