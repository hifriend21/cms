import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Parameter } from '@workout-cms/api-interfaces';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ParametersService {
  model = 'param';

  constructor(private http: HttpClient) {}

  create(createRequest: Parameter): Observable<Parameter> {
    return this.http.post<Parameter>(`${this.getUrl()}`, createRequest);
  }

  update(updateRequest: any): Observable<void> {
    const id = updateRequest.uuid;
    return this.http.put<void>(`${this.getUrlWithId(id)}`, updateRequest);
  }

  getAll(): Observable<Parameter[]> {
    return this.http.get<Parameter[]>(`${this.getUrl()}`);
  }

  getById(id: string): Observable<Parameter> {
    return this.http.get<Parameter>(`${this.getUrlWithId(id)}`);
  }

  delete(id: number): Observable<void> {
    return this.http.delete<void>(`${this.getUrlWithId(id)}`);
  }

  private getUrl() {
    return `${environment.apiEndpoint}${this.model}`;
  }

  private getUrlWithId(id) {
    return `${this.getUrl()}/${id}`;
  }
}
