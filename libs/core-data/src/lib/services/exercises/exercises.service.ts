import { HttpClient, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Exercise, ExerciseDetails } from '@workout-cms/api-interfaces';
import { Observable } from 'rxjs';

export interface CreateExerciseRequest {
  image: string;
  practice_video: string;
  technique_video: string;
  number: number;
  parent_id: number;
  to_point_id: number;
  from_point_id: number;
  entry_video?: string;
  exit_video?: string;
  translations: {
    [key: string]: {
      description: string;
      name: string;
      start_audio?: string;
      middle_audio?: string;
      end_audio?: string;
      technique_audio?: string;
      exit_audio?: string;
      entry_audio?: string;
    };
  };
}

@Injectable({
  providedIn: 'root',
})
export class ExercisesService {
  model = 'exercise';

  constructor(private http: HttpClient) {}

  create(createRequest: CreateExerciseRequest): Observable<HttpEvent<any>> {
    return this.http.post<void>(`${this.getUrl()}`, createRequest, {
      reportProgress: true,
      observe: 'events',
    });
  }

  update(updateRequest: FormData): Observable<void> {
    const id = updateRequest.get('id');
    return this.http.put<void>(`${this.getUrlWithId(id)}`, updateRequest);
  }

  getAll(): Observable<Exercise[]> {
    return this.http.get<Exercise[]>(`${this.getUrl()}`);
  }

  getById(id: string): Observable<ExerciseDetails> {
    return this.http.get<ExerciseDetails>(`${this.getUrlWithId(id)}`);
  }

  delete(id: number): Observable<void> {
    return this.http.delete<void>(`${this.getUrlWithId(id)}`);
  }

  private getUrl() {
    return `${environment.apiEndpoint}${this.model}`;
  }

  private getUrlWithId(id) {
    return `${this.getUrl()}/${id}`;
  }
}
