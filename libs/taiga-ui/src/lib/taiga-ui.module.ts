import { NgModule } from '@angular/core';
import {
  TuiRootModule,
  TuiButtonModule,
  TuiFormatNumberPipeModule,
  TuiSvgModule,
  iconsPathFactory,
  TUI_ICONS_PATH,
  TuiLoaderModule,
} from '@taiga-ui/core';
import { TuiInputModule, TuiInputPasswordModule } from '@taiga-ui/kit';
import {
  TuiTableModule,
  TuiTablePaginationModule,
} from '@taiga-ui/addon-table';
import { TuiLetModule } from '@taiga-ui/cdk';
import { TUI_LANGUAGE, TUI_RUSSIAN_LANGUAGE } from '@taiga-ui/i18n';
import { of } from 'rxjs';

@NgModule({
  exports: [
    TuiRootModule,
    TuiButtonModule,
    TuiTableModule,
    TuiInputModule,
    TuiInputPasswordModule,
    TuiFormatNumberPipeModule,
    TuiLetModule,
    TuiSvgModule,
    TuiTablePaginationModule,
    TuiLoaderModule,
  ],
  providers: [
    {
      provide: TUI_ICONS_PATH,
      useValue: iconsPathFactory('assets/taiga-ui/icons/'),
    },
    {
      provide: TUI_LANGUAGE,
      useValue: of(TUI_RUSSIAN_LANGUAGE),
    },
  ],
})
export class TaigaUiModule {}
