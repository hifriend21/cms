import { Component } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '@workout-cms/core-data';

@Component({
  selector: 'workout-cms-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  readonly form: FormGroup = this.fb.group({
    email: [null, [Validators.required, Validators.email]],
    password: new FormControl('password', Validators.required),
  });
  constructor(
    private fb: FormBuilder,
    private readonly authService: AuthService,
    private router: Router
  ) {}

  get f(): { [key: string]: AbstractControl } {
    return this.form.controls;
  }

  login() {
    const loginRequest = {
      email: this.f.email.value,
      password: this.f.password.value,
    };
    this.authService.login(loginRequest).subscribe((result) => {
      this.authService.setToken(result['token']);
      // Redirect to home
      this.router.navigateByUrl('/');
    });
  }
}
